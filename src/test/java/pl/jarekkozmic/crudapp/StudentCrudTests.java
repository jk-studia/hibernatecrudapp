package pl.jarekkozmic.crudapp;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.jarekkozmic.crudapp.cruds.StudentCrud;
import pl.jarekkozmic.crudapp.entity.Student;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import static org.junit.jupiter.api.Assertions.*;


public class StudentCrudTests{

    private static Managable managable;
    private static StudentCrud studentCrud;

    @BeforeAll
    public static void setUp(){
        managable = new Manager("/META-INF/hibernateTest.cfg.xml");
        studentCrud = new StudentCrud(managable);
        Session session = managable.configureSessionFactory().openSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            Query query = session.createSQLQuery("insert into student(studentName, studentNumber, studentSurname) values " +
                                                    "('Jarek',123456, 'Kozmic');");
            query.executeUpdate();
            query =  session.createSQLQuery("insert into student(studentName, studentNumber, studentSurname) values " +
                    "('Marek',654321, 'Koza');");
            query.executeUpdate();
            transaction.commit();
        } catch (HibernateException he){
            if(transaction!=null) transaction.rollback();
            he.printStackTrace();
        } finally {
            session.close();
        }
    }


    @Test
    public void createStudentTest(){
        Student student = new Student("Maciek", "Kowalski", 456123);
        studentCrud.createStudent(student);
        Student student1 = studentCrud.getStudentByNumber(student.getStudentNumber());
        assertEquals(student, student1);
    }


    @Test
    public void updateStudentTest(){
        Student student = studentCrud.getStudentByNumber(123456);
        student.setStudentName("Jan");
        student.setStudentSurname("Kowalski");
        studentCrud.updateStudent(student);
        Student student1 = studentCrud.getStudentByNumber(123456);
        assertEquals(student.getStudentName(), student1.getStudentName());
        assertEquals(student.getStudentSurname(), student1.getStudentSurname());
    }

    @Test
    public void deleteStudentTest(){
        Student student = studentCrud.getStudentByNumber(654321);
        studentCrud.deleteStudent(student);
        assertThrows(NoResultException.class, ()->studentCrud.getStudentByNumber(654321));
    }

    @Test
    public void readStudentTableTest(){
        assertDoesNotThrow(()->studentCrud.readStudentTable());
    }

    @Test
    public void getStudentTest(){
        assertNotNull(studentCrud.getStudentByNumber(123456));
    }

    @AfterAll
    public static void tearDown(){
        Session session = managable.configureSessionFactory().openSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            Query query = session.createSQLQuery("drop tables if exists student, university");
            query.executeUpdate();
            transaction.commit();
        } catch (HibernateException he){
            if(transaction!=null) transaction.rollback();
            he.printStackTrace();
        } finally {
            session.close();
        }
    }
}
