package pl.jarekkozmic.crudapp;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.jarekkozmic.crudapp.cruds.UniversityCrud;
import pl.jarekkozmic.crudapp.entity.Student;
import pl.jarekkozmic.crudapp.entity.University;

import static org.junit.jupiter.api.Assertions.*;

import javax.persistence.NoResultException;
import javax.persistence.Query;

public class UniversityCrudTests {

    private static Managable managable;
    private static UniversityCrud universityCrud;
    @BeforeAll
    public static void setUp(){
        managable = new Manager("/META-INF/hibernateTest.cfg.xml");
        universityCrud = new UniversityCrud(managable);
        Session session = managable.configureSessionFactory().openSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            Query query = session.createSQLQuery("insert into university(universityName) value ('AGH')");
            query.executeUpdate();

            query = session.createSQLQuery("insert into university(universityName) value ('UEK')");
            query.executeUpdate();

            query = session.createSQLQuery("insert into student(studentName, studentNumber, studentSurname) values " +
                    "('Jarek',123456, 'Kozmic');");
            query.executeUpdate();
            query =  session.createSQLQuery("insert into student(studentName, studentNumber, studentSurname) values " +
                    "('Marek',654321, 'Koza');");
            query.executeUpdate();


            transaction.commit();
        } catch (HibernateException he){
            if(transaction!=null) transaction.rollback();
            he.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Test
    public void createUniversityTest(){
        University university = new University("UJ");
        universityCrud.createUniversity(university);
        University university1 = universityCrud.getUniversityByName("UJ");
        assertEquals(university, university1);
    }

    @Test
    public void updateUniversityTest(){
        University university = universityCrud.getUniversityByName("AGH");
        university.setUniversityName("PK");
        university.getStudentSet().add(new Student("Jan", "Kowalski", 789456));
        universityCrud.updateUniversity(university);
        University university1 = universityCrud.getUniversityByName("PK");
        assertEquals(university, university1);
        assertEquals(university.getStudentSet(), university1.getStudentSet());
    }

    @Test
    public void readUniversityTableTest(){
        assertDoesNotThrow(()->universityCrud.readUniversityTable());
    }

    @Test
    public void deleteUniversityTest(){
        University university = universityCrud.getUniversityByName("UEK");
        universityCrud.deleteUniversity(university);
        assertThrows(NoResultException.class, ()->universityCrud.getUniversityByName("UEK"));
    }

    @AfterAll
    public static void tearDown(){
        Session session = managable.configureSessionFactory().openSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            Query query = session.createSQLQuery("drop tables if exists student, university");
            query.executeUpdate();
            transaction.commit();
        } catch (HibernateException he){
            if(transaction!=null) transaction.rollback();
            he.printStackTrace();
        } finally {
            session.close();
        }
    }
}
