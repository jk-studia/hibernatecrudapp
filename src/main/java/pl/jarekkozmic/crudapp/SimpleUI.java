package pl.jarekkozmic.crudapp;

import pl.jarekkozmic.crudapp.cruds.StudentCrud;
import pl.jarekkozmic.crudapp.cruds.UniversityCrud;
import pl.jarekkozmic.crudapp.entity.Student;
import pl.jarekkozmic.crudapp.entity.University;

import java.util.Scanner;

public class SimpleUI {

    private Managable managable;
    private StudentCrud studentCrud;
    private UniversityCrud universityCrud;

    public SimpleUI(Managable managable) {
        this.managable = managable;
        studentCrud = new StudentCrud(managable);
        universityCrud = new UniversityCrud(managable);
    }

    private void studentCRUDUI(Scanner scanner){
        Student student;
        System.out.println("Choose CRUD option");
        System.out.println("1. Create student");
        System.out.println("2. Read table");
        System.out.println("3. Update student");
        System.out.println("4. Delete student");
        int choice = scanner.nextInt();
        switch (choice){
            case 1:
                student = new Student();
                System.out.println("Set name:");
                student.setStudentName(scanner.next());
                System.out.println("Set surname:");
                student.setStudentSurname(scanner.next());
                System.out.println("Set number: ");
                student.setStudentNumber(scanner.nextInt());
                studentCrud.createStudent(student);
                System.out.println("Student has been created");
                break;
            case 2:
                studentCrud.readStudentTable();
                break;
            case 3:
                System.out.println("Number of student to be modified:");
                student = studentCrud.getStudentByNumber(scanner.nextInt());
                System.out.println("Set name:");
                student.setStudentName(scanner.next());
                System.out.println("Set surname:");
                student.setStudentSurname(scanner.next());
                System.out.println("Set number: ");
                student.setStudentNumber(scanner.nextInt());
                studentCrud.updateStudent(student);
                System.out.println("Student has been modified");
                break;
            case 4:
                System.out.println("Number of student to be deleted:");
                student = studentCrud.getStudentByNumber(scanner.nextInt());
                studentCrud.deleteStudent(student);
                System.out.println("Student has been deleted");
                break;
            default:
                System.out.println("Wrong choice");
        }
    }

    private void universityCRUDUI(Scanner scanner){
        University university;
        System.out.println("Choose CRUD option");
        System.out.println("1. Create university");
        System.out.println("2. Read table");
        System.out.println("3. Update university");
        System.out.println("4. Delete university");
        int choice = scanner.nextInt();
        switch (choice) {
            case 1:
                university = new University();
                System.out.println("Set name:");
                university.setUniversityName(scanner.next());
                universityCrud.createUniversity(university);
                break;
            case 2:
                universityCrud.readUniversityTable();
                break;
            case 3:
                System.out.println("Name of university be modified:");
                university = universityCrud.getUniversityByName(scanner.next());
                System.out.println("Set name:");
                university.setUniversityName(scanner.next());
                universityCrud.updateUniversity(university);
                break;
            case 4:
                System.out.println("Name of university to be deleted");
                university = universityCrud.getUniversityByName(scanner.next());
                universityCrud.deleteUniversity(university);
                break;
            default:
                System.out.println("Wrong choice");
        }
    }

    public void run(){
        int choice;
        Scanner scanner = new Scanner(System.in);
        for(;;){
            System.out.println("Choose CRUD table");
            System.out.println("1. Student");
            System.out.println("2. University");
            System.out.println("0. Close app");
            choice = scanner.nextInt();
            switch (choice){
                case 1: studentCRUDUI(scanner); break;
                case 2: universityCRUDUI(scanner); break;
                case 0:
                    if(((Manager)managable).getSessionFactory()!=null)
                        managable.closeSessionFactory();
                    scanner.close();
                    System.exit(0);
                default:
                    System.out.println("Wrong choice");
            }

        }
    }
}
