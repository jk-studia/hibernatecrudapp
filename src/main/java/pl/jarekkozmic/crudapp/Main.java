package pl.jarekkozmic.crudapp;



import javafx.stage.Stage;

public class Main {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        Managable managable = new Manager("/META-INF/hibernate.cfg.xml");
        SimpleUI simpleUI = new SimpleUI(managable);
        simpleUI.run();
    }

}
