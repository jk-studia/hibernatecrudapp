package pl.jarekkozmic.crudapp.entity;

import javax.persistence.*;

@Entity
@Table(name = "student")
public class Student {
    private int studentID;
    private String studentName;
    private String studentSurname;
    private int studentNumber;

    public Student(){}

    public Student(String studentName, String studentSurname, int studentNumber){
        this.studentName = studentName;
        this.studentSurname = studentSurname;
        this.studentNumber = studentNumber;
    }

    /**
     *
     * @return student id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    @Column(name = "studentName", length = 45, nullable = false)
    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    @Column(name = "studentSurname", length = 45, nullable = false)
    public String getStudentSurname() {
        return studentSurname;
    }

    public void setStudentSurname(String studentSurname) {
        this.studentSurname = studentSurname;
    }

    @Column(name = "studentNumber", unique = true, nullable = false)
    public int getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(int studentNumber) {
        this.studentNumber = studentNumber;
    }


    @Override
    public int hashCode(){
        return studentNumber;
    }

    @Override
    public boolean equals(Object o){
        if(this==o)
            return true;

        if(!(o instanceof  Student))
            return false;

        Student check = (Student) o;

        if(check.getStudentNumber()==this.getStudentNumber())
            return true;
        else
            return false;
    }

    @Override
    public String toString(){
        return "Student ID: "+studentID+", name and surname: "+studentName+" "+studentSurname+", number: "+studentNumber;
    }

}
