package pl.jarekkozmic.crudapp.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@Entity
@Table(name = "university")
public class University {

    private int universityID;
    private String universityName;
    private Set<Student> studentSet = new HashSet<Student>();

    public University() {
    }

    public University(String universityName) {
        this.universityName = universityName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    public int getUniversityID() {
        return universityID;
    }

    public void setUniversityID(int universityID) {
        this.universityID = universityID;
    }

    @Column(name = "universityName", length = 100, unique = true, nullable = false)
    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "universityID")
    public Set<Student> getStudentSet() {
        return studentSet;
    }

    public void setStudentSet(Set<Student> studentList) {
        this.studentSet = studentList;
    }

    public String showStudents(){
        StringBuffer stringBuffer = new StringBuffer();
        Iterator iterator = studentSet.iterator();
        while(iterator.hasNext()){
            stringBuffer.append("\n   -").append(iterator.next());
        }
        return stringBuffer.toString();
    }

    @Override
    public int hashCode(){
        return universityName.hashCode();
    }

    @Override
    public boolean equals(Object o){
        if(o==this)
            return true;
        if(!(o instanceof University))
            return false;

        University check = (University) o;
        if(check.getUniversityName().equals(this.getUniversityName()))
            return true;
        else
            return false;
    }

    @Override
    public String toString(){
        return "University ID: "+universityID+", name: "+universityName+", students: "+showStudents();
    }
}
