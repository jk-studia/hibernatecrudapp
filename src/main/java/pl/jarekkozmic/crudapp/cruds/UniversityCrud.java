package pl.jarekkozmic.crudapp.cruds;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.jarekkozmic.crudapp.Managable;
import pl.jarekkozmic.crudapp.entity.University;

import javax.persistence.Query;

public class UniversityCrud {
    private Managable manager;
    private ICrud<University> crud;

    public UniversityCrud(Managable manager) {
        this.manager = manager;
        crud = new Crud<>(manager, "University");
    }

    public void createUniversity(University university){
        crud.create(university);
    }

    public void readUniversityTable(){
        crud.read();
    }

    public void updateUniversity(University university){
        crud.update(university);
    }

    public void deleteUniversity(University university){
        crud.delete(university);
    }


    /*
       While deleting object which was obtained by this method, hibernate will also delete associated Students in studentSet
     */
    public University getUniversityByName(String name){
        Session session = manager.configureSessionFactory().openSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            Query query = session.createQuery("from University university left join fetch university.studentSet where university.universityName = :name");
            query.setParameter("name", name);
            transaction.commit();
            return (University) query.getSingleResult();
        } catch(HibernateException he){
            if(transaction!=null) transaction.rollback();
            he.printStackTrace();
            return null;
        } finally{
            session.close();
        }
    }
}