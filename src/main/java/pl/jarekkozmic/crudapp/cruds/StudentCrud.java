package pl.jarekkozmic.crudapp.cruds;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.jarekkozmic.crudapp.Managable;
import pl.jarekkozmic.crudapp.entity.Student;

import javax.persistence.Query;


public class StudentCrud {
    private Managable manager;
    private ICrud<Student> crud;

    public StudentCrud(Managable manager) {
        this.manager = manager;
        crud = new Crud<>(manager, "Student");
    }
    public void createStudent(Student student){
        crud.create(student);
    }

    public void readStudentTable(){
        crud.read();
    }

    public void updateStudent(Student student){
        crud.update(student);
    }

    public void deleteStudent(Student student){
        crud.delete(student);
    }


    public Student getStudentByNumber(int studentNumber){
        Session session = manager.configureSessionFactory().openSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Student where studentNumber = :studentNumber");
            query.setParameter("studentNumber", studentNumber);
            transaction.commit();
            return (Student) query.getSingleResult();
        } catch(HibernateException he) {
            if(transaction!=null) transaction.rollback();
            he.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }


    public Managable getManager() {
        return manager;
    }
}
