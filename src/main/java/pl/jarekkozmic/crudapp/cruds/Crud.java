package pl.jarekkozmic.crudapp.cruds;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.jarekkozmic.crudapp.Managable;

import javax.persistence.Query;
import java.util.Iterator;
import java.util.List;

public class Crud<Entity> implements ICrud<Entity>{

    private Managable manager;
    private String tableName;
    public Crud(Managable manager, String tableName){
        this.manager = manager;
        this.tableName = tableName;
    }

    public void create(Entity objectToPersist){
        Session session = manager.configureSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(objectToPersist);
            session.flush();
            transaction.commit();
        } catch(HibernateException he) {
            if(transaction!=null) transaction.rollback();
            he.printStackTrace();
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public void read() {
        Session session = manager.configureSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from "+tableName);
            List<Entity> list = query.getResultList();
            Iterator iterator = list.iterator();
            while(iterator.hasNext()){
                System.out.println(iterator.next());
            }
            transaction.commit();
        } catch(HibernateException he) {
            if(transaction!=null) transaction.rollback();
            he.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void update(Entity objectToUpdate){
        Session session = manager.configureSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(objectToUpdate);
            transaction.commit();
        } catch(HibernateException he) {
            if(transaction!=null) transaction.rollback();
            he.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void delete(Entity objectToDelete){
        Session session = manager.configureSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.delete(objectToDelete);
            transaction.commit();
        } catch(HibernateException he) {
            if(transaction!=null) transaction.rollback();
            he.printStackTrace();
        } finally {
            session.close();
        }
    }


    public void setManager(Managable manager){
        this.manager = manager;
    }
}
