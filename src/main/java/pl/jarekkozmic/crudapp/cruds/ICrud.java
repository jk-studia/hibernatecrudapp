package pl.jarekkozmic.crudapp.cruds;

public interface ICrud<Entity> {
     void create(Entity objectToCreate);
     void read();
     void update(Entity objectToUpdate);
     void delete(Entity objectToDelete);
}
