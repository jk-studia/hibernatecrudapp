package pl.jarekkozmic.crudapp;

import org.hibernate.SessionFactory;

public interface Managable {
    SessionFactory configureSessionFactory();
    void closeSessionFactory();
}
