package pl.jarekkozmic.crudapp;

import org.hibernate.SessionFactory;

import org.hibernate.cfg.Configuration;

public class Manager implements Managable {
    private String configurationFile;
    public Manager(String configurationFile){
        this.configurationFile = configurationFile;
    }
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public SessionFactory configureSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.configure(configurationFile);

        try {
            sessionFactory = configuration.buildSessionFactory();
        }catch(Throwable ex){
            System.out.println("Could not build factory" + ex);
            throw new ExceptionInInitializerError(ex);
        }
        return sessionFactory;
    }

    @Override
    public void closeSessionFactory(){
        sessionFactory.close();
    }
}
