drop table if exists student;
drop table if exists university;

create table university(
	id int auto_increment unique not null,
    universityName varchar(100) unique not null,
    primary key(id)
);

create table student(
	id int auto_increment not null unique,
	studentName varchar(45) not null,
    studentSurname varchar(45) not null,
    studentNumber int unique not null, 
    universityID int,
    primary key(id)
    foreign key(universityID) references university(id)
);

