# HibernateCrudApp

Simple CRUD application for Hibernate framework.

Technologies used in project:

1. Database: MySQL
2. Hibernate core
3. JUnit5 for testing